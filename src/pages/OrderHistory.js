import '../styles/OrderHistory.css';
import { Fragment, useEffect, useState, useContext } from 'react';
import Container from '../components/Container';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faReceipt, faTruck, faFlagCheckered } from '@fortawesome/free-solid-svg-icons'; 
import { Button, Badge } from 'react-bootstrap';
import UserContext from '../userContext';

export default function OrderHistory(){

	const { user } = useContext(UserContext);

	const token = localStorage.getItem('token');
	const [products, setProducts] = useState([]);
	const [showTransaction, setShowTransaction] = useState('');
	const [itemPay, setItemPay] = useState('');
	const monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"
	];

	let month,year, date, comment_date, emptyList;

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders',{
			method: 'GET',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			setProducts(data);
		});		
	}, [])


	const userItemPay = products.filter(item => item.userId === user.id && item.status === 'checked-out')


	const toPayFunc = () => {
		if(userItemPay.length > 0){
			const payList = userItemPay.map(details => {
				let prodDate = new Date(details.createdOn)
				month = monthNames[prodDate.getMonth()];
				year = prodDate.getFullYear();
				date = prodDate.getDate();
				comment_date = `${month} ${date}, ${year}`;
				return <div key={details._id} className="product__pay-list">
					<div>
						<p className="order-history__prodName">{details.productName}</p>
						<p className="order-history__date">{comment_date }</p>
					</div>
						<p className="order-history__qty">{details.qty}</p>
					<p className="order-history__status">{details.status}</p>
				</div>					
			})
			setItemPay(payList)
			setShowTransaction('PAY');
		}
	}

	const toShipFunc = () => {
		setShowTransaction('SHIP');
		const userItem = products.filter(item => item.userId === user.id && item.status === 'shipped')
		console.log(userItem.length)
		if(userItem.length > 0){}else{
			return <p>Empty</p>
		}
	}

	const toReceiveFunc = () => {
		setShowTransaction('RECEIVE');
		const userItem = products.filter(item => item.userId === user.id && item.status === 'received')
		console.log(userItem.length)
		if(userItem.length > 0){}else{
			return <p>Empty</p>
		}
	}
	return <Fragment>
		<Container>
			<div className="order-history__container">
				<p className="order-history__Header">Orders</p>
				<div className="order-history__process-container">
					<div className="order-history__toPayBox">
						<FontAwesomeIcon icon={faReceipt} className="order-history__icon"/>
						{(userItemPay.length > 0)? <Badge pill variant="danger" className="badge-icon-orderHistory">{userItemPay.length}</Badge>:null}
						<Button variant="light" onClick={toPayFunc} className="order-history__process">To Pay</Button>
					</div>
					<div className="order-history__toShipBox">
						<FontAwesomeIcon icon={faTruck} className="order-history__icon"/>
						<Button variant="light" className="order-history__process" onClick={toShipFunc}>To Ship</Button>
					</div>
					<div className="order-history__toReceiveBox">
						<FontAwesomeIcon icon={faFlagCheckered} className="order-history__icon"/>
						<Button variant="light" className="order-history__process" onClick={toReceiveFunc}>To Receive</Button>
					</div>
				</div>
			</div>
			{(showTransaction === 'PAY')? 
				<div className="order-history__box">
				<p className="order-history__Header">Products</p>
				<div className="order-history__subHeader">
					<p className="order-history__subHeaders">Product Name</p>
					<p className="order-history__subHeaders">Quantity</p>
					<p className="order-history__subHeaders">Status</p>
				</div>
				{itemPay}
				</div>
				: null}
			
		</Container>
	</Fragment>
}