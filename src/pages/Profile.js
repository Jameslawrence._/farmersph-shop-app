import '../styles/Profile.css';
import { Fragment, useState, useEffect, useContext } from 'react';
import Container from '../components/Container';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEdit, faInfo  } from '@fortawesome/free-solid-svg-icons'; 
import { Button } from 'react-bootstrap';
import FormUpdate from '../components/FormUpdate';
import ChangePassword from '../components/ChangePassword';
import DisableAccount from '../components/DisableAccount';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';


export default function Profile(){
	const {user, counter, setCounter} = useContext(UserContext);

	const [ address, setAddress ] = useState([]);
	const [ fullName, setFullName] = useState([]);
	const [ info, setInfo ] = useState([]);
	const [ updateBtn, setUpdateBtn ] = useState(false);
	const [ disableForm, setDisableForm ] = useState(false);
	const [ showChangePassForm, setShowChangePassForm ] = useState(false);
	const [ userInfo, setUserInfo ] = useState({
		id:'',
		firstname:'', 
		lastname:'',
		lotNumber:'',
		barangay:'',
		city:'',
		province:'',
		country:'',
	});

	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

	useEffect(() => {
	    let token = localStorage.getItem('token');
	    fetch('https://safe-bastion-71965.herokuapp.com/user/profile',{
	      method: 'GET',
	      headers: { Authorization: `Bearer ${token}`}
	    })
		.then(res => res.json())
		.then(data => {
			if(data.auth !== 'failed'){
			setUserInfo({})
			setUserInfo(() => {
				return {
					id: data._id,
					firstname: data.fullName[0].givenName,
					lastname: data.fullName[0].familyName,
					lotNumber:data.address[0].lotNumber,
					barangay:data.address[0].barangay,
					city:data.address[0].city,
					province:data.address[0].province,
					country:data.address[0].country,
				}
			})
			const now = new Date(data.birthDate);
			const bday = (months[now.getMonth()] + ' ' + now.getDate() + ' ' + now.getFullYear())
			setAddress(data.address.map((address,index) => {
				 return <div key={index} className="address-box">
					<p className="personal-title">Address:</p>
					<span className="personal__info">{address.lotNumber} {address.barangay} {address.city} {address.province} {address.country}</span>
				</div>
			}))
			setFullName(data.fullName.map((name,index) => {
				return <div key={index} className="address-box">
					<p className="personal-title">Name: </p>
					<span className="personal__info">{name.givenName} {name.familyName}</span>
				</div>
			}))
			setInfo(() => {
				return <div>
					<div  className="address-box">
						<p className="personal-title">Email:</p>
						<span className="personal__info">{data.email}</span>
					</div>
					<div  className="address-box">
						<p className="personal-title">Gender:</p>
						<span className="personal__info">{data.gender}</span>
					</div>
					<div  className="address-box">
						<p className="personal-title">Birthday:</p>
						<span className="personal__info">
						{bday}
						</span>
					</div>
				</div>
			})			
		}});
    },[counter])

	const showDisableFormFunc = () => {
		if(disableForm) setDisableForm(false)
		else setDisableForm(true)
	}

	const onUpdateForm = () => {
		if(updateBtn) setUpdateBtn(false)
		else setUpdateBtn(true)
	}
	const handleChange = (e) => {
		const {value, name} = e.target;
		console.log(value)
		return setUserInfo(prevValue => {
			return {...prevValue, [name]: value}
		})
	}
	const showChangePassFormFunc = () => {
		if(showChangePassForm) setShowChangePassForm(false)
		else setShowChangePassForm(true)
	}
	return <Fragment>
			{(user.id === null)? <Navigate to='/login'/>:
			<Container>
			<div className="manage-account__box">
			<div className="manage-account__box-header">
				<p className="manage-pesonal__header">Manage My Account</p>
				<Button variant="light" onClick={onUpdateForm}><FontAwesomeIcon icon={faEdit}/></Button>
			</div>
				<p className="manage-pesonal__sub-header">Personal Details</p>
				{fullName}
				{info}
				<p className="manage-pesonal__sub-header">Address Book</p>
				{address}
			</div>

			{(updateBtn)? <div className="manage-personal__form-update"><FormUpdate values={userInfo} onHandleChange={handleChange} updateBtn={setUpdateBtn} /></div>:null}
			<div className="manage-account__box">
				<p className="manage-pesonal__header">Secure Account</p>
				<div className="security-card">
					<FontAwesomeIcon className="icon-info" icon={faInfo}/>
					<small> To be more secured please change your password every <span className="changePass-month">3 months</span>. </small>
					{showChangePassForm? null: <Button variant="dark" onClick={showChangePassFormFunc} className="security-changePass">Change Password</Button>}
					<ChangePassword onShow={showChangePassForm} setOnShow={setShowChangePassForm} values={userInfo}/>
				</div>
				<div className="security-card">
					<FontAwesomeIcon className="icon-info" icon={faInfo}/>
					<small> Disabling your account we will make sure that your data is safe with us and to enable the account you can login.</small>
					{(disableForm)? null :<Button onClick={showDisableFormFunc} variant="dark" className="security-changePass">Disable Account</Button>}
					{(disableForm)? <DisableAccount onShowDisableForm = {setDisableForm} values={userInfo}/>: null}
				</div>
			</div>
		</Container>}
		</Fragment>
}
