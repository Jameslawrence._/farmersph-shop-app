import {useState, useEffect, Fragment} from 'react';
import Table from '../components/TopSellingTable';


export default function TopSelling () {

	const [item, setItem] = useState([]);

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/product',{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(item => {
			console.log(item) 

			const topProducts = item.filter(product => product.purchaser.length >=1).sort().reverse();
			console.log(topProducts);

			setItem(topProducts.map(items => {

				return (
					<Table keyProducts = {items.id} topItems = {items}/>
					)
			}));
		})

		

	}, [])

	return (
		<Fragment>
			{item}
		</Fragment>
	)
}