import '../styles/Cart.css';
import {Fragment,useContext, useState, useEffect} from 'react';
import UserContext from '../userContext';
import Container from '../components/Container';
import {Button, Image}  from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import {Link} from 'react-router-dom';
export default function Cart(){
	const {user, counter, setCounter} = useContext(UserContext);
	const userId = user.id
	const token = localStorage.getItem('token')



	const [products, setProducts] = useState([]);
	const [itemsLength, setItemsLength] = useState(0);
	const [itemView, setItemView] = useState('');
	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders',{
			method: 'GET',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			setProducts(data)
			const items = data.filter(item => item.userId === userId)
			setItemsLength(items.length)
		});		
	}, [counter])
	const items = products.filter(item => item.userId === userId && item.status === 'pending');



	const removeItem = (e) => {
		const orderId = e.target.value; 
		fetch(`https://safe-bastion-71965.herokuapp.com/user/delete-order/${orderId}`,{
			method: 'DELETE',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setCounter(counter + 1)
			console.log(data)
		})
	}

	useEffect(()=> {
		setItemView(items.map(item => {
			return <div> 
			<div key={item._id} className="cart-container">
				<div className="cart-header">
					<p className="cart-header__prodName">{item.productName}</p>
					<p className="cart-header__quantity">x{item.qty} pcs</p>
				</div>
				<div className="cart-header__priceHolder">
					<p className="cart-header__price">₱ {item.price}</p>
					<Button onClick={removeItem} value={item._id} variant="light" className="removeIcon"><FontAwesomeIcon icon={faTimes} className="remove-icon"/></Button>
				</div>
			</div>
			
			</div>
		}))		
	},[products])

	return <Container>
		<div className="cart-view">
			<p className="cart-view-header">Added to Cart</p>
			{itemView.length > 0? itemView: 
				<Fragment>
					<div className="cart-empty-box">
						<p className="cart-empty-box__title">Oops!</p>
						<p className="cart-empty-box__subTitle">Your cart is empty. Buy Now.</p>
						<Image src='./images/emptyCart.svg' fluid className="cart-empty-img"></Image>
						<Button as={Link} to='/products' variant="dark" exact='true' className="cart-empty-box__btn">Products</Button>
					</div>
				</Fragment>
			}
			{itemView.length > 0? <Button as={Link} to='/checkout' exact='true' className="cart-view__chkOutBtn">Proceed to Checkout</Button>: null}
		</div>
	</Container>

}