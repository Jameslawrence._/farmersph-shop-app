import { Fragment, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEye  } from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom'; 
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function DisableAccount({onShowDisableForm, values}){
	const {unsetUser, setUser} = useContext(UserContext);
	const history = useNavigate()
	const {id} = values
	const [userInput, setUserInput] = useState({password:''});
	const [passwordType, setPasswordType] = useState('password');

	const { password } = userInput;

	const handleChange = (e) => {
		const {name, value} = e.target
		setUserInput(prevValue => {return {...prevValue, [name]:value}})
	}

	const showPasswordFunc = () => {
		if(passwordType === 'password') setPasswordType('text')
		else setPasswordType('password')
	}
	const SuccessAlertUpdate = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Account Disabled',
			icon: 'success',
			confirmButtonText: 'Close'
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to disable account.',
			icon: 'error',
			confirmButtonText: 'Close'
		})
	}		
	const disableAccount = (e) => {
		e.preventDefault();
		let token = localStorage.getItem('token');

		fetch(`https://safe-bastion-71965.herokuapp.com/user/validate-password/${id}`, {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				password: password
			})	
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				fetch(`https://safe-bastion-71965.herokuapp.com/user/disable/${id}`,{
					method: 'PATCH',
					headers:{
						'Content-Type':'application/json',
						Authorization: `Bearer ${token}`
					},
					body: JSON.stringify({
						isActive: false 
					})
				})	
				.then(res => res.json())
				.then(data => {
					SuccessAlertUpdate()
					onShowDisableForm(false)
					unsetUser();
					setUser({
				      id: null,
				      firstname: null,
				      lastname: null,
				      isAdmin: null
					})
					history('/login');
				})	
			}else{
				ErrorAlert()
			}
		})
	}
	
	return <Fragment>
		<Form>
			<Form.Group className="mb-3">
			<Form.Label className="form-label-header">Password</Form.Label>
			<div className="password-container">
			<Form.Control 
				onChange={handleChange}
				size="sm" 
				type={passwordType}
				name="password"
				values={password}
				placeholder="Please enter your last name" />
			<Button onClick={showPasswordFunc} value="passwordBtn" size="sm" className="form-button__seePassword"><FontAwesomeIcon icon={faEye}/></Button>
			</div>
			</Form.Group>
			<Button variant="dark" onClick={()=> onShowDisableForm(false)} className="changePasswordBtn">Cancel</Button>	
			<Button variant="danger" onClick={disableAccount} disabled={password?false:true} className="changePasswordBtn">Disable Account</Button>	
		</Form>	
	</Fragment>		
}