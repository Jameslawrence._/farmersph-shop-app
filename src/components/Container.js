import '../styles/Container.css';

export default function Container(props){
	const classes = 'container-box '+ props.className; 
	return <div className={ classes }>{ props.children }</div>
}