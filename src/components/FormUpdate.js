import '../styles/FormUpdate.css';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useContext } from 'react';
import UserContext from '../userContext';

export default function FormUpdate(props){
	const { id, firstname, lastname, lotNumber, barangay, city, province, country } = props.values
	const {counter, setCounter} = useContext(UserContext);

	let disableBtn = true;
	if(firstname && lastname && lotNumber && barangay && city && country){
		disableBtn = false
	}else disableBtn = true
	

	const SuccessAlertUpdate = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Successfully updated profile',
			icon: 'success',
			showConfirmButton:false
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to create the account',
			icon: 'error',
			confirmButtonText: 'Try again'
		})
	}

	const updateUserInfo = () => {
		fetch(`https://safe-bastion-71965.herokuapp.com/user/update-info/${id}`,{
			method: 'PATCH',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				firstname:firstname,
				lastname:lastname,
				lotNumber:lotNumber,
				barangay:barangay,
				city:city,
				province:province,
				country:country,				
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				SuccessAlertUpdate()
				props.updateBtn(false)
				setCounter(counter + 1)
			}else{
				ErrorAlert()
			}

		})
	}


	return	<Form>
		<p className="update-form__header">Edit Profile</p>
		<p className="update-form__subHeader">*All fields are required! Do not leave blanks!</p>
		<Form.Group className="mb-3" controlId="formBasicEmail">
	    <Form.Label className="form-label-header">First Name</Form.Label>
	    <Form.Control 
	    	onChange={props.onHandleChange} 
	    	size="sm" 
	    	type="text"
	    	name="firstname"
	    	value= {firstname}
	    	placeholder="Please enter your first name" />
	  </Form.Group>
	  <Form.Group className="mb-3" controlId="formBasicEmail">
	    <Form.Label className="form-label-header">Last Name</Form.Label>
	    <Form.Control 
	    	onChange={props.onHandleChange}
	    	size="sm" 
	    	type="text"
	    	name="lastname" 
	    	value= {lastname}
	    	placeholder="Please enter your last name" />
	  </Form.Group>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label className="form-label-header">Address</Form.Label>
				    <div className="signup-address-input">
				    <Form.Control  
				    	onChange={props.onHandleChange}
				    	className="lotNumber" 
				    	size="sm" 
				    	type="text" 
				    	name="lotNumber"
			    		value= {lotNumber}
				    	placeholder="Lot number" />
				    <Form.Control  
				    	onChange={props.onHandleChange} 
				    	className="barangay" 
				    	size="sm" 
				    	type="text" 
				    	name="barangay"
			    		value= {barangay}
				    	placeholder="Barangay" />
			    </div>
			    <div className="signup-country">
				    <Form.Control  
				    	onChange={props.onHandleChange} 
				    	size="sm" 
				    	type="text" 
				    	name="city"
			    		value= {city}
				    	placeholder="City" />
				    <Form.Control  
				    	onChange={props.onHandleChange}  
				    	size="sm" 
				    	type="text" 
				    	name="province"
			    		value= {province}
				    	placeholder="Province" />
				    <Form.Control  
				    	onChange={props.onHandleChange}
				    	size="sm" 
				    	type="text" 
				    	name="country"
			    		value= {country}
				    	placeholder="Country" />
			    </div>
			  </Form.Group>
			  <div className="form-update__btnProcessContainer">
			  <Button variant="danger" className="form-update__cancelBtn" onClick={()=>{ props.updateBtn(false)}}>cancel</Button>
			  <Button variant="warning" className="form-update__updateBtn" onClick={updateUserInfo} disabled={disableBtn? true: false}>save changes</Button>
			  </div>
  </Form>
}